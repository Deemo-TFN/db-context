﻿namespace dbconText.Models
{
    public class Transactions
    {
        public int TransactionsId { get; set; }
        public int EmployeeId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }

        public Reports? Reports { get; set; }

        public string AddTextHere { get; set; }

    }
}
