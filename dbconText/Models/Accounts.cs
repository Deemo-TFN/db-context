﻿namespace dbconText.Models
{
    public class Accounts
    {
        public string AccountId { get; set; }
        public string CustomerId { get; set; }
        public string AccountName { get; set; }

        public Reports? Reports { get; set; }

        public string AddTextHere { get; set; }
    }
}
